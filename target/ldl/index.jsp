<!DOCTYPE html>
<html>
<%@ page contentType="text/html;charset=UTF-8" %>
<style type="text/css">
    #connect-container {
        float: left;
        width: 400px
    }

    #connect-container div {
        padding: 5px;
    }

    #console-container {
        float: left;
        width: 400px;
    }

    #console {
        border: 1px solid #CCCCCC;
        border-right-color: #999999;
        border-bottom-color: #999999;
        height: 270px;
        overflow-y: scroll;
        padding: 5px;
        width: 100%;
    }

    #console p {
        padding: 0;
        margin: 0;
    }
</style>
<script>
    var URL = "ws://localhost:8080/WebSocketChatRoom/chatRoomServer";
    var websocket;
    var userName;

    function setConnected(connected) {
        document.getElementById('connect').disabled=connected;
        document.getElementById('disconnect').disabled = !connected;
        document.getElementById('send').disabled = !connected;
    }

    function connect() {

        if('WebSocket' in window){
            websocket = new WebSocket(URL);
        }else if('MozWebSocket' in window){
            websocket = new MozWebSocket(URL);
        }else{
            alert("您的浏览器不支持WebSocket,请更换最新版本浏览器");
            return;
        }

        websocket.onopen = function (evnt) {
            userName = document.getElementById('userId').value;
            if(userName == ""){
                alert("用户名不能为空");
                return;
            }else{
                setConnected(true);
                websocket.send(userName + "加入聊天室");
            }
        };
        websocket.onmessage = function (evnt) {
            onMessage(evnt)
        };
        websocket.onerror = function (evnt) {
            onError(evnt)
        };
        websocket.onclose = function (evnt) {
            disconnect(evnt);
        }
    }
    function sendMessage() {
        var userName = document.getElementById('userId').value;
        var msg = document.getElementById('message').value;
        websocket.send(userName+": "+msg);

    }
    function onMessage(evnt) {
        if (typeof evnt.data == "string") {
            log(evnt.data);
        }
    }
    function onError(evnt) {
        log('错误: ' + evnt.data);
    }
    /*function onClose(evnt) {
        setConnected(false);
    }*/
    function disconnect(evnt) {
        if(websocket != null){
            websocket.close(1000, userName + "退出聊天室");
            websocket = null;
            log("你已退出聊天室");
            setConnected(false);
        }

    }
    function log(message) {
        var console = document.getElementById('console');
        var p = document.createElement('p');
        p.style.wordWrap = 'break-word';
        p.appendChild(document.createTextNode(message));
        console.appendChild(p);
        while (console.childNodes.length > 25) {
            console.removeChild(console.firstChild);
        }
        console.scrollTop = console.scrollHeight;
    }
</script>
<body>
<noscript><h2 style="color: #ff0000">请刷新页面加载Javascript</h2></noscript>
<div>
    <div id="connect-container">
        <div>
            <span id="msg"></span>
            <label>用户名:</label><input id="userId" type="text" name="userName" value="" size="10">
            <button id="connect" onclick="connect();">连接</button>
            <button id="disconnect" onclick="disconnect();" disabled="true">断开</button>
        </div>
        <div id="console-container">
            <div id="console"></div>
        </div>
        <div>


        </div>
        <div>

            <textarea id="message" style="width: 350px" placeholder="输入内容"></textarea>
        </div>
        <div>
            <button id="send" onclick="sendMessage()" disabled="true">发送</button>
        </div>
        <div>
        </div>
    </div>
    <%--<div id="console-container">
        <div id="console"></div>
    </div>--%>
</div>
</body>
</html>
